import React, {Component} from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Typography from '@material-ui/core/Typography';

export default class Appbar extends Component {
  componentDidMount() {
                                
  }
  render() {
      return (
        <div className="main-wrap">
          <AppBar position="static">
            <Toolbar>
              <Typography className="w-100 text-center" variant="h6">
                Project Name
              </Typography>
              <IconButton edge="start" color="inherit" aria-label="menu">
                <MenuIcon />
              </IconButton>
              <Typography className=" time" variant="h6">
                11:30 AM
              </Typography>
            </Toolbar>
          </AppBar>
        </div>
        
      );
  }
}
