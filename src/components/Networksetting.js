import React, {Component}  from 'react';
import Grid from '@material-ui/core/Grid';
import Header from '../components/header/Header';
import $ from 'jquery';

export default class NetworkSetting extends Component  {
  componentDidMount() {
    $('.network-digit').keydown(function (e) {
      if (e.shiftKey || e.ctrlKey || e.altKey) { // if shift, ctrl or alt keys held down
          e.preventDefault();         // Prevent character input
      } else {
          var n = e.keyCode;
          if (!((n == 8)              // backspace
          || (n == 46)                // delete
          || (n >= 35 && n <= 40)     // arrow keys/home/end
          || (n >= 48 && n <= 57)     // numbers on keyboard
          || (n >= 96 && n <= 105))   // number on keypad
          ) {
              e.preventDefault();     // Prevent character input
          }
      }
  });
  $("#confirm").click(function(){
    var u = 'true';
    var ip=$("#network_address1").val()+'.'+$("#network_address2").val()+'.'+$("#network_address3").val()+'.'+$("#network_address4").val();
    var subnet=$("#subnet_mask1").val()+'.'+$("#subnet_mask2").val()+'.'+$("#subnet_mask3").val()+'.'+$("#subnet_mask4").val();
    if(ip.length<12){
      $(".error_network_address").css('border','1px solid red');
      u = 'false';
    }else{
      $(".error_network_address").css('border','1px solid #fff');
      u = 'true';
    }
    if(subnet.length<12){
      $(".error_subnet_mask").css('border','1px solid red');
      u = 'false';
    }else{
      $(".error_subnet_mask").css('border','1px solid #fff');
      u = 'true';
    }
    // if ((ip === undefined) || (ip === null) || (ip.length > 15)){ 
    //   console.log('are deva'); 
    //   u='false';
    // }
    // var p = ip.split('.');
    // if (p.length != 4){ 
    //   console.log('are deva');
    //   u='false';
    // }
    // p.forEach( function(v,k){
    //   p[k]=Number(v);
    // });
    // if (isNaN(p[0]) || isNaN(p[1]) || isNaN(p[2]) || isNaN(p[3]) ){ 
    //   console.log('are deva');
    //   u='false';
    // }
    // if ((p[0] < 1) || (p[0] > 255) || (p[1] < 0) || (p[1] > 255) || (p[2] < 0) || (p[2] > 255) || (p[3] < 0) || (p[3] > 255)){ 
    //   console.log('are deva');
    //   u='false';
    // }
    if (u!='false'){ window.location.href='./project';}    
  });                
  }
   render() {
  return (
    <div>
      <Grid container spacing={1}>
        <Header />
        <Grid item xs={12} className="mt-4">
          <Grid className="container-fluid" container spacing={1}>
            <Grid item xs={6} md={6}>
              <label className="text-white net-lable"> Network Address: </label>
            </Grid>
            <Grid item xs={6} md={6}>
              
            </Grid>
            <Grid item xs={4} md={4}>
              
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="network_address1" className="net-in w-100 error_network_address network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="network_address2" className="net-in w-100 error_network_address network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="network_address3" className="net-in w-100 error_network_address network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="network_address4" className="net-in w-100 error_network_address network-digit" maxlength="3" />
            </Grid>
          </Grid>
        </Grid>
        <Grid item xs={12} className="mt-4">
          <Grid className="container-fluid" container spacing={1}>
            <Grid item xs={6} md={6}>
              <label className="text-white net-lable"> Subnet Mask: </label>
            </Grid>
            <Grid item xs={6} md={6}>
              
            </Grid>
            <Grid item xs={4} md={4}>
              
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="subnet_mask1" className="net-in w-100 error_subnet_mask network-digit"  pattern="\d*" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="subnet_mask2" className="net-in w-100 error_subnet_mask network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="subnet_mask3" className="net-in w-100 error_subnet_mask network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={2} md={2}>
              <input type="text" id="subnet_mask4" className="net-in w-100 error_subnet_mask network-digit" maxlength="3" />
            </Grid>
            <Grid item xs={12} md={12} className="mt-5">
              <button className="btn btn-default" id="confirm">Confirm</button>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
}
}