import React, {Component} from 'react';
import AppBar from '../../components/appbar/Appbar';
import Footer from '../../components/footer/Footer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import SemiCircleProgressBar from "react-progressbar-semicircle";
import whiteArrow from '../../assets/white-arrow.png';
import powerbutton from '../../assets/power.png';
import tempicon from '../../assets/temp.png';



export default class Fridgemonitoring extends Component {
  componentDidMount() {
   
  }
  
  render() {
    const percentage = 50;
      return (
        <div className="main-wrap">
          <AppBar />
          <Grid container spacing={1} className="main-chiller-monitoring">
            <Grid item xs={12}>
                <Typography className="text-center text-white mt-2 monitor-title" variant="h6" align="center">
                  Fridge 1 
                </Typography>
            </Grid>
            <Grid item xs={12} md={6} className="progressbar-sec2">
                <Grid container spacing={1} className="main-chiller-monitoring mt-5">
                  <Grid item xs={12} md={6} className="fridgetemprature mt-2">
                      <SemiCircleProgressBar   percentage={50} strokeWidth={25} showPercentValue={false} />
                      <Typography className="text-white water-temp" variant="h6" align="center">
                        3.8<span className="temp-p3">°C</span>
                        
                      </Typography>
                      <Typography className="text-center text-white temp-text" variant="h6" align="center">
                      <img className="mt-2" src={tempicon} alt="tempicon"/><br />
                         Temprature
                      </Typography>
                  </Grid>
                  <Grid item xs={12} md={6}>
                      <Typography className="text-center text-white mycard power-sec m-2" variant="div" align="center">
                        <img className="mt-5" src={powerbutton} alt="powerbutton"/>
                        <Typography className="text-center text-white mt-5" variant="h5" align="center">
                            Status: STAND BY
                        </Typography>
                      </Typography>
                  </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} md={6} className="mt-4">
                <Typography className="text-center text-white" variant="div" align="center">
                  <ul className="main-chiller-listing">
                    <li>Turn On/Off <img src={whiteArrow} alt="rightarrow"/>  </li>
                    <li>Status  <img src={whiteArrow} alt="rightarrow"/></li>
                    <li>Set Point <img src={whiteArrow} alt="rightarrow"/></li>
                    <li>Door Status  <img src={whiteArrow} alt="rightarrow"/></li>
                    <li>Trapped Man  <img src={whiteArrow} alt="rightarrow"/></li>
                  </ul>
                </Typography>
            </Grid>
          </Grid>
          <Footer />
        </div>
        
      );
  }
}
