import React, {Component} from 'react';
import AppBar from '../../components/appbar/Appbar';
import Footer from '../../components/footer/Footer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import SemiCircleProgressBar from "react-progressbar-semicircle";
import whiteArrow from '../../assets/white-arrow.png';


export default class Mainchiller extends Component {
  componentDidMount() {
   
  }
  
  render() {
    const percentage = 66;
      return (
        <div className="main-wrap">
          <AppBar />
          <Grid container spacing={1} className="main-chiller-monitoring">
            <Grid item xs={12}>
                <Typography className="text-center text-white mt-2" variant="h6" align="center">
                  Main Chiller Monitoring 
                </Typography>
            </Grid>
            <Grid item xs={12} md={6} className="progressbar-sec">
                <Grid container spacing={1} className="main-chiller-monitoring mt-5">
                  <Grid item xs={12} md={6}>
                      <SemiCircleProgressBar  percentage={60} strokeWidth={25} showPercentValue />
                      <Typography className="text-center text-white mt-5" variant="h5" align="center">
                        Speed %
                      </Typography>
                  </Grid>
                  <Grid item xs={12} md={6} className="temprature">
                      <SemiCircleProgressBar   percentage={10} strokeWidth={25} showPercentValue={false} />
                      <Typography className="text-white water-temp" variant="h6" align="center">
                        10 <span className="temp-p">.5</span><span className="temp-p2">°C</span>
                      </Typography>
                      <Typography className="text-center text-white temp-text" variant="h6" align="center">
                        Circulation <br/> Water Temprature
                      </Typography>
                  </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12} md={6} className="mt-4">
                <Typography className="text-center text-white mt-2" variant="div" align="center">
                  <ul className="main-chiller-listing">
                    <li>General Chiller Status <img src={whiteArrow} alt="rightarrow"/>  </li>
                    <li>Chiller 1: Running <br /> Speed: 60 rps  <img src={whiteArrow} alt="rightarrow"/></li>
                    <li>Chiller 2: Disabled <br /> Speed: 0 rps <img src={whiteArrow} alt="rightarrow"/></li>
                    <li>General Chiller Status  <img src={whiteArrow} alt="rightarrow"/></li>
                  </ul>
                </Typography>
            </Grid>
          </Grid>
          <Footer />
        </div>
        
      );
  }
}
