import React, {Component} from 'react';
import AppBar from '../../components/appbar/Appbar';
import Footer from '../../components/footer/Footer';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Hublogo from '../../assets/logo1.png';
import Hublogo2 from '../../assets/logo2.png';
import chiller from '../../assets/chiller-white.png';
import cabins from '../../assets/cabins.png';
import fridge from '../../assets/fridge-white.png';
import room from '../../assets/room-white.png';
import {
  Link
} from "react-router-dom";
export default class Project extends Component {
  componentDidMount() {
                                
  }
  render() {
      return (
        <div className="main-wrap">
          <AppBar />
          <Grid container spacing={1} className="ProjectCard-sec justify-content-center">
            <Grid item xs={12}>
              <div className="logo-inner">
                <img src={Hublogo} alt="Hublogo"/>
              </div>
            </Grid>
            <Grid item xs={12} md={4} className="ProjectCard-item">
              <Link to="/Mainchiller">
                <div className="ProjectCard">
                  <img src={chiller} alt="Hublogo2"/>
                  <Typography className="text-center text-white mt-2" variant="h6" align="center">
                    Main Chiller  Monitoring
                  </Typography>
                </div>
              </Link>
            </Grid>
            <Grid item xs={12} md={4} className="ProjectCard-item">
              <Link to="/Mainchiller">
                <div className="ProjectCard">
                  <img src={cabins} alt="Hublogo2"/>
                  <Typography className="text-center text-white mt-2" variant="h6" align="center">
                    Cabin's Tempratures
                  </Typography>
                </div>
              </Link>
            </Grid>
            <Grid item xs={12} md={4} className="ProjectCard-item">
              <Link to="/Mainchiller">
                <div className="ProjectCard">
                  <img src={chiller} alt="Hublogo2"/>
                  <Typography className="text-center text-white mt-2" variant="h6" align="center">
                      Cool-only Chiller Monitoring
                  </Typography>
                </div>
              </Link>
            </Grid>
            <Grid item xs={12}>
              <div className="logo-inner">
                <img src={Hublogo2} alt="Hublogo2"/>
              </div>
            </Grid>
            <Grid item xs={12} md={4} className="ProjectCard-item">
              <Link to="/Fridgemonitor">
                <div className="ProjectCard">
                  <img src={fridge} alt="fridge"/>
                  <Typography className="text-center text-white mt-2" variant="h6" align="center">
                    Fridge<br /> Monitoring
                  </Typography>
                </div>
              </Link>
            </Grid>
            <Grid item xs={12} md={4} className="ProjectCard-item">
              <div className="ProjectCard">
                <img src={room} alt="room"/>
                <Typography className="text-center text-white mt-2" variant="h6" align="center">
                  Cold Room Monitoring
                </Typography>
              </div>
            </Grid>
          </Grid>
          <Footer />
        </div>
        
      );
  }
}
