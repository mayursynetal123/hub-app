import React, {Component} from 'react';
import Grid from '@material-ui/core/Grid';
import footericon1 from '../../assets/f-icon1.png';
import footericon2 from '../../assets/f-icon2.png';
import footericon3 from '../../assets/f-icon3.png';
import footericon4 from '../../assets/f-icon4.png';
import footericon5 from '../../assets/f-icon5.png';
import footericon6 from '../../assets/f-icon6.png';
import footericon7 from '../../assets/f-icon7.png';
import footericon8 from '../../assets/f-icon8.png';


export default class Footer extends Component {
  render() {
      return (
          <Grid container spacing={1} className="footer-nav justify-content-center">
            <Grid item xs={12} item md={6} >
              <Grid container spacing={1}>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon1} alt="Hublogo2"/>
                  </div>
                </Grid>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon2} alt="Hublogo2"/>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} item md={4} >
              <Grid container spacing={1}>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon3} alt="Hublogo2"/>
                  </div>
                </Grid>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon4} alt="Hublogo2"/>
                  </div>
                </Grid>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon5} alt="Hublogo2"/>
                  </div>
                </Grid>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon6} alt="Hublogo2"/>
                  </div>
                </Grid>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon7} alt="Hublogo2"/>
                  </div>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={12} item md={2} >
              <Grid container spacing={1}>
                <Grid item xs={12} item md={2} >
                  <div className="footerNav-icons">
                    <img src={footericon8} alt="Hublogo2"/>
                  </div>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        
      );
  }
}
