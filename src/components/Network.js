import React, {Component} from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Header from '../components/header/Header';

export default class Network extends Component {

  componentDidMount() {
   const textShuffle = function(element, text, timer){
      var thisEl = document.getElementById(element),
        counter = 0,
        t = setInterval(function(){		
            if(counter == text.length -1){
                t = window.clearInterval(t);
            }
            setTimeout(function(){
              var htm = text[counter];
              htm+='<button class="btn btn-default mt-4"><a href="/networksetting" class="text-white">Change Local Network Setting</a></button>';
                thisEl.innerHTML = htm;
                counter++;						
            },310);
        }, timer);
      }
    var shuffle1 = new textShuffle('hubnotfound', 
                                   ['Hub not found ! <br>'],
                                   5500);
                    
                                
  }
  render() {
      return (
        <div className="main-wrap">
        <Grid container spacing={1}>
          <Header />
          <Grid item xs={12}>
          <Typography className="text-center w-100" variant="p" align="center">
            <CircularProgress className="text-white" />
          </Typography>
          <Typography className="text-center text-white mt-5" variant="h6" align="center">
            Looking For A Nearby <br /> HUB...<br /><span id='hubnotfound'></span>
          </Typography>
          </Grid>
        </Grid>
    </div>
        
      );
  }
}
