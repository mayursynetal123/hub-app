import './App.css';
import Network from './components/Network'
import NetworkSetting from './components/Networksetting';
import Project from './components/project/Project';
import Mainchiller from './components/project/Mainchiller';
import Fridgemonitor from './components/project/Fridgemonitor';

import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={Network} />
          <Route path="/networksetting" component={NetworkSetting} />
          <Route path="/project" component={Project} />
          <Route path="/mainchiller" component={Mainchiller} />
          <Route path="/fridgemonitor" component={Fridgemonitor} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
